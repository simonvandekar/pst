#!/appl/R-3.1.3/bin/Rscript --vanilla --no-init-file
#### LIBRARIES ####
# to load nifti images
suppressMessages(require(optparse))



### ARGUMENTS ###
ol = list(make_option(c("-i", "--imagedata"), action="store", default=NULL, help="Path to 4D nifti image where each image is a volume for each subject. Images are assumed to be in the same order as data file."),
	    make_option(c("-b", "--basis"), action="store", default=NULL, help="Basis for space to maximize the score statistic. Can be a 4D nifti image, an atlas image where each region is indicated with a different integer, or an integer. If an integer, b, is given the b-dimensional basis is constructed from a PCA of the imaging data."),
	    make_option(c("-l", "--label"), action="store_true", default=FALSE, help="Include this flag if the basis is a 3d label image."),
	    make_option(c("-d", "--datafile"), action="store", default=NULL, help="Data file (either csv or RDS) that contains phenotype/outcome variable and any covariates to control for. Can have extra columns that won't be used in the analysis. Assumed to be in the same order as imagedata."),
	    make_option(c("-e", "--equation"), action="store", default=NULL, help="Model equation for covariates in standard R form \"outcome ~ covariate1 + ... + covariatem\". If there are no covariates then specify \"outcome ~ 1\". Covariate and outcome names must match column names in datafile given above."),
	    make_option(c("-f", "--family"), action="store", default='gaussian', help="Family for outcome variable. See \"?family\" in R. Known to work for binomial, gaussian, and quasipoisson. Default is gaussian."),
	    make_option(c("-n", "--nsim"), action="store", default=5000, help="Number of simulations for Monte Carlo integration."),
	    make_option(c("-m", "--mask"), action="store", default=NULL, help="Optional mask to use for analysis. Providing a mask will greatly improve computing time. A mask is recommended for all analyses and required if basis argument is an integer."),
	    make_option(c("-o", "--output"), action="store", default=NULL, help="Path to where output should be placed.")
)

#### LOAD ORO.NIFTI ####
suppressMessages(require(oro.nifti))

# copies hdr from img1 to img2
# for writing out files in the correct orientation
copyhdr = function(img1, img2){
atts = c("pixdim","qform_code","sform_code","quatern_b","quatern_c","quatern_d","qoffset_x","qoffset_y","qoffset_z","srow_x","srow_y","srow_z","reoriented")
attributes(img2)[atts] = attributes(img1)[atts]
img2
}

#### FUNCTIONS ####
maxsum.posthoc = function(m, GQ, Q, nsimint=5000, alpha=0.05){
	nsimint=nsimint - 1
	Y = resid(m, type='response')
	X = model.matrix(m)
	Gamma = diag(Y^2)
	covY = ( Gamma - Gamma %*% X %*% solve( t(X) %*% Gamma %*% X) %*% t(X) %*% Gamma)
	r = ncol(Q)
	# Things for the variance
	V = t(GQ) %*% covY %*% GQ
	Vsqrt = svd(V, nv=0, nu=r)
	Vsqrt = Vsqrt$u %*% diag(sqrt(Vsqrt$d), r) %*% t(Vsqrt$u)
	QVsqrt = Q %*% Vsqrt
	# sqrt of diagonal of Q V Q^T (covariance of the projected U vector)
	diagcov = sqrt(rowSums(QVsqrt^2))
	diagcov = ifelse(diagcov==0, 1, diagcov)


	# Projected scores
	PU = Q %*% t(GQ) %*% Y
	# projected scores are standardized here.
	PUstd = PU/diagcov

	infnorm = function(z){
	  max(abs(QVsqrt %*% z)/diagcov) # divide by variances
	}

	# Monte carlo integration -- or just simulation under the null
	cat('performing simulations\n')
	simnormals = matrix(rnorm(nsimint*r), ncol=r, nrow=nsimint)
	siminftynorms = apply(simnormals, 1, infnorm)
	# add observed data plus small epsilon to avoid log10(0)
	Fb = ecdf(c(siminftynorms, max(abs(PUstd))+10^{-8} ) )
	# define a quantile function and compute voxelwise pvalues (and return the result)
	list(PUstd = PUstd, rejreg = quantile(siminftynorms, probs=1-alpha), pvals = 1-Fb(abs(PUstd)), Fb=Fb )
}

#### POSTHOC INFERENCE FOR LABELS ####
# this is faster than for any other basis
# because the problem is more simple with indicator bases
maxsum.posthoc.label = function(m, GQ, r, nsimint=5000, alpha=0.05){
	# actual value is one of the "simulated" values
	nsimint=nsimint-1
	Y = resid(m, type='response')
	X = model.matrix(m)
	Gamma = diag(Y^2)
	covY = ( Gamma - Gamma %*% X %*% solve( t(X) %*% Gamma %*% X) %*% t(X) %*% Gamma )
	# Things for the variance
	V = t(GQ) %*% covY %*% GQ
	# Vsqrt is standardized here
	Vsqrt = svd(cov2cor(V), nv=0, nu=r)
	Vsqrt = Vsqrt$u %*% diag(sqrt(Vsqrt$d), r) %*% t(Vsqrt$u)
	# sqrt of diagonal of Q V Q^T (covariance of the projected U vector)
	diagcov = sqrt(diag(V))
	diagcov = ifelse(diagcov==0, 1, diagcov)

	# observed mean values in each region
	PU = t(GQ) %*% Y
	# These are technically rotated scores, not projected
	PUstd = c(PU/diagcov)

	# Vsqrt is standardized
	infnorm = function(z){
	  max(abs(Vsqrt %*% z))
	}

	# Monte carlo integration -- or just simulation under the null
	cat('performing simulations\n')
	simnormals = matrix(rnorm(nsimint*r), ncol=r, nrow=nsimint)
	siminftynorms = apply(simnormals, 1, infnorm)
	# add observed data plus small epsilon to avoid log10(0)
	Fb = ecdf(c(siminftynorms, infnorm(PUstd) + 10^{-8}) )
	# define a quantile function and compute voxelwise pvalues (and return the result)
	return(list(PUstd = PUstd, rejreg = quantile(siminftynorms, probs=1-alpha), pvals = 1-Fb(abs(PUstd)), Fb=Fb ))
}



opt = parse_args(OptionParser(option_list=ol))
# try to convert basis to number, if it works, then assume it is not an image
suppressWarnings(basisnum <- as.numeric(opt[['basis']]))
if( !is.na(basisnum) ){
	opt$basis = basisnum
}
cat(str(opt))

#opt = list()
#opt$basis = '~/maxsum/maxsum_cmdline/basis.nii.gz'
#opt$equation = 'dx ~ 1'
#opt$family='binomial'
#opt$datafile = '~/maxsum/maxsum_cmdline/4d/n446_datafile.csv'
#opt$imagedata = "~/maxsum/maxsum_cmdline/4d/n446_ravens_sm8_4d.nii.gz"
#opt$mask = "~/maxsum/maxsum_cmdline/4d/n446_ravens_sm8_mask.nii.gz"
#opt$output = "~/maxsum/maxsum_cmdline/test"
#opt$label=FALSE
#opt$nsim=5000

#### CREATE OUTPUT DIRECTORY ####
out = opt$output
dir.create(out, showWarnings=FALSE, recursive=TRUE)
# newly created images
out.pval = file.path(out, 'negative_log10_pvalue.nii.gz')
out.zval = file.path(out, 'standardized_projected_scores.nii.gz')
# save these images
out.basis = file.path(out, 'basis.nii.gz')
out.mask = file.path(out, 'mask.nii.gz')
# text file for statistical output
out.stats = file.path(out, 'maxsum_test.txt')
# rdata file for R objects of interest
out.rdata = file.path(out, 'useful_objects.rdata')


#### FILE AND ARGUMENT CHECKS ####
if( is.null( opt[[ 'imagedata' ]] ) ){
	stop('Must include input data.')
}

# check that data and mask (if given) exist
images = c('imagedata', 'mask')
if( any(! file.exists(unlist(opt[images])[sapply(opt[images], is.null)]) ) ){
	stop('Mask or image data don\'t exist.')
}

# check that basis image exists (if given)
if( ! is.numeric(opt$basis) ){
	if( ! file.exists(opt$basis)){
		stop("Basis/label image doesn't exists.")
	}
}

# check image dimensions
# uses c3d and parses standard output
images = 'imagedata'
# check if a mask was passed
if( !is.null(opt$mask)){
	images = c(images, 'mask')
}
# check if the basis command is an image
if( !is.numeric(opt$basis)){
	images = c(images, 'basis')
}
# check if image dimensions are all the same
cmd = paste('c3d', paste(c(rbind(unlist(opt[ images ]), '-info')), collapse=' '), '| cut -d ";" -f1 | cut -d":" -f2')
dims = system(cmd, intern=TRUE)
# removes white space
dims = dims[ grep("^$", dims, invert=TRUE)]
if(length(unique(dims)) > 1){
	stop(paste('Images dimensions do not all match.', paste(dims, collapse=';')) )
}


# read in data file
if(grepl('.csv$', tolower(opt$datafile))){
	dat = read.csv(opt$datafile)
}else if(grepl('.rds$', tolower(opt$datafile)) ){
	dat = readRDS(opt$datafile)
}else{
	stop('I don\'t know how to read this file!')
}

# 4D subject file
cat('****** LOADING IMAGING DATA ******\n')
imgloadtime = system.time(imgdat <- readNIfTI( opt$imagedata))
cat('Data load time:', round(as.table(imgloadtime)[3]/60, 2), 'minutes\n', sep=' ')

# masks image
if( ! is.null(opt$mask)){
	system(paste('cp', opt$mask, out.mask))
	maskdat = readNIfTI( opt$mask)
	# replaces nifti object with vector of voxels within mask
	imgdat = apply(imgdat@.Data, 4, c)[ c(maskdat@.Data)!=0, ]
}

# 4D basis file of where the time dimension is $r$. Or, an atlas file
# where regions in the image are indicated by integers. Or, an integer 
# specifying the rank of the PCA basis.
# replaces nifti object with vector of voxels within mask
# loads basis image and masks it
if( ! is.numeric(opt$basis)){
	system(paste('cp', opt$basis, out.basis))
	basisdat = readNIfTI( opt$basis)
	# MASK
	if( ! is.null(opt$mask)){
		# replaces nifti object with vector of voxels within mask
		if(length(dim(basisdat))==4){
			basisdat = apply(basisdat@.Data, 4, c)[ c(maskdat@.Data)!=0, ]
		}else{
			basisdat = c(basisdat)[ c(maskdat@.Data)!=0]
		}
	}
}

#### NULL MODEL ####
# does not involve imaging data
f0 = as.formula(opt$equation)
m0 = glm(f0, data=dat, family=opt$family)

#### PCA basis ####
if( is.numeric(opt$basis)){
	r = opt$basis
	# voxels are rows
	cat('****** PERFORMING SVD ******\n')
	svdtime = system.time(imgsvd <- svd(scale(imgdat, center=TRUE, scale=FALSE), nu=r, nv=r))
	cat('Time to perform SVD:', round(as.table(svdtime)[3]/60, 2), 'minutes\n', sep=' ')
	# V = Q^T G^T G Q, G^T=Q D W^T, GQ =WD
	s = imgsvd$v %*% diag(imgsvd$d[1:r])
	basisdat = imgsvd$u
	rm(imgsvd)
	# maxsum posthoc procedure
	cat('****** PERFORMING MS POSTHOC ******\n')
	posthoctime = system.time(ms.posthoc <- maxsum.posthoc( m=m0, GQ=s, Q=basisdat, nsimint=opt$nsim ))
	cat('Time to perform MS posthoc:', round(as.table(posthoctime)[3]/60, 2), 'minutes\n', sep=' ')

	# alternative model
	f1 = update.formula(f0, '. ~ . + s')
	m1 = glm(f1, data=dat, family=opt$family)
	ms.test = anova(m0, m1, test='Rao')
}


#### CUSTOM BASIS ####
if( ! opt$label ){
	r = ncol(basisdat)
	# might want to make an option to check orthogonormality of the basis matrix
	basisdat = sweep(basisdat, 2, sqrt(colSums(basisdat^2)), '/')
	# checks orthonormality
	cat('****** CHECKING BASIS MATRIX ******\n')
	ident = round(t(basisdat) %*% basisdat, digits=6)
	if(all(ident[lower.tri(ident)]==0) & all(diag(ident) == 1 ) ){
		s = t(imgdat) %*% basisdat
		# maxsum posthoc procedure
		cat('****** PERFORMING MS POSTHOC ******\n')
		posthoctime = system.time(ms.posthoc <- maxsum.posthoc( m=m0, GQ=s, Q=basisdat, nsimint=opt$nsim ))
		cat('Time to perform MS posthoc:', round(as.table(posthoctime)[3]/60, 2), 'minutes\n', sep=' ')

		# alternative model
		f1 = update.formula(f0, '. ~ . + s')
		m1 = glm(f1, data=dat, family=opt$family)
		ms.test = anova(m0, m1, test='Rao')
	}else{
		stop('Basis matrix does not have orthonormal columns.')
	}
}

#### ANATOMICAL BASIS ####
if( opt$label & !is.numeric(opt$basis) ){
	# removes 0
	uq = sort(unique(basisdat))[-1]
	# check if all integers
	if(all(uq == round(uq) ) ){
		r = length(uq)
		# maxsum posthoc procedure
		# GQ is mean in region multiplied by square root of number of
		# voxels in the region, this command ignores the square root term
		s = sapply(uq, function(uqi){ colMeans( imgdat[which(basisdat==uqi), ]) } )
		cat('****** PERFORMING MS POSTHOC ******\n')
		posthoctime = system.time(ms.posthoc <- maxsum.posthoc.label( m=m0, GQ=s, r=r, nsimint=opt$nsim ) )
		cat('Time to perform MS posthoc:', round(as.table(posthoctime)[3]/60, 2), 'minutes\n', sep=' ')

		# alternative model
		f1 = update.formula(f0, '. ~ . + s')
		m1 = glm(f1, data=dat, family=opt$family)
		ms.test = anova(m0, m1, test='Rao')

		# write images
		cat('****** WRITING OUTPUT ******\n')
		cmds = c(paste('c3d', opt$basis, '-replace', paste(c(rbind(uq, sign(ms.posthoc$PUstd) * -log10(ms.posthoc$pvals ) )), collapse=' '), '-o', out.pval ),
				paste('c3d',opt$basis, '-replace', paste( c(rbind(uq, sign(ms.posthoc$PUstd) * -log10(ms.posthoc$pvals ))), collapse=' ' ), '-o', out.zval ) )
		trash = sapply(cmds, system)
	}else{
		stop('Basis is a label, but the image has noninteger values.')
	}
}

if( ! opt$label ){
	cat('****** WRITING OUTPUT ******\n')
	#### THESE ALL WRITE OUT WRONG
	# write out pvalue image
	imgout = array(0, dim=dim(maskdat))
	imgout[ maskdat@.Data!=0 ] = sign(ms.posthoc$PUstd) * -log10(ms.posthoc$pvals)
	imgout = nifti(imgout, datatype=16)
	imgout = copyhdr(maskdat, imgout)
	writeNIfTI(imgout, rmniigz(out.pval))

	# write out projected score image
	imgout = array(0, dim=dim(maskdat))
	imgout[ maskdat@.Data!=0 ] = ms.posthoc$PUstd
	imgout = nifti(imgout, datatype=16)
	imgout = copyhdr(maskdat, imgout)
	writeNIfTI(imgout, rmniigz(out.zval))

	# write out basis 4d image
	imgout = maskdat@.Data
	imgout = array(apply(basisdat, 2, function(b){ imgout[ maskdat@.Data!=0 ] = b; imgout} ), dim=c(dim(maskdat), r))
	imgout = nifti(imgout, datatype=16)
	imgout = copyhdr(maskdat, imgout)
	writeNIfTI(imgout, rmniigz(out.basis))
}

# stats output
capture.output(ms.test, file=out.stats, append=FALSE)
save(m0, m1, ms.test, ms.posthoc, file=out.rdata)
