# maxkat_power_results. Just combines the power results
# into one file. Will probably also create some plots.
# Assumes you ran maxkat_altsim_setup.R
# in the same directory because it uses setupfile variable
# and other variables defined from running that script.
powdir = './power_results/'
getfwer=TRUE
maxsumdir = file.path(powdir, 'vertexwise_power')
dir.create(maxsumdir, showWarnings=FALSE)
setupfile = './power_files/setup_n100_modellogistic_sigma1_alpha0.05_bunif0-0.rdata'
load(setupfile)
source('maxsum_functions.R')

# load first one to get the format
voxelwiseb = which(betas[,1]==0)
load(sub('setup_', '', sub('.rdata', paste('_beta', voxelwiseb, '_simid', 1, '.rdata', sep=''), sub('power_files', 'power_results', setupfile) ) ) )
		fullsimresults =  matrix(NA, nrow=nsim, ncol=ncol(simresults))
		colnames(fullsimresults) = names(simresults)

# compiles results
for(b in 1:nrow(betas)){
	cat(b, '\n')
	outfile = sub('setup_', '', sub('.rdata', paste('_beta', b, '.rdata', sep=''), sub('power_files', 'power_results', setupfile) ) )
	if( ! file.exists(outfile)){
		fullpval.list = array(pval.list, c(dim(pval.list), nsim) )
		pb <- txtProgressBar(style=3)
		for(s in 1:nsim){
			rm(list=c('simresults', 'pval.list'))
			load(sub('setup_', '', sub('.rdata', paste('_beta', b, '_simid', s, '.rdata', sep=''), sub('power_files', 'power_results', setupfile) ) ) )
		fullsimresults[s,] = unlist(simresults[1,])
		# add voxelwise pvalues to array
		fullpval.list[,,s] = pval.list
		setTxtProgressBar(pb, round(s/nsim, 2))
		}
		save(list=c('fullsimresults', 'fullpval.list'), file=outfile )
	}
	load(outfile)
	powresults[b,] = c(colMeans(fullsimresults[,grep('_pvalue$', nams)]<=alpha, na.rm=TRUE), b)
}
fullpow = as.data.frame(powresults)
subpow = fullpow
subpow = subpow[ order(subpow$beta),]
subpow = subpow[ , grep('DK', names(subpow), invert=TRUE)]
names(subpow) = c('aPCA', paste('PCA', rs), paste('Atlas', drs), 'aSPU', 'SKAT', 'Sum', 'SPU Inf', 'beta')
subpow = subpow[ ! subpow$beta %in% c(3, 10), ]
# reorder columns to make sense
subpow = subpow[, c(1:8, 10, 12, 9, 13)]


### PDF file for omnibus test
pdffile = sub('rdata', 'pdf', sub('setup_', '', setupfile))
#shapes = expand.grid(1:6, 1:10)
#shapes = cbind(c(rep(1, length(rs)), rep(0,length(drs)), rep(2, ncol(subpow) - length(rs) - length(drs) -1) ), c(1:length(rs), 1:length(drs), (length(rs)+2):(ncol(subpow) -length(drs)) )  )
shapes = cbind(c(rep(1, 4), rep(0, 4), rep(2, 3)), c( 1:4, 1:4, 5,6, 1) )
pdf(pdffile, height=8, width=8)
cbPalette <- c("#000000", "#E69F00", "#56B4E9", "#009E73",  "#0072B2", "#D55E00", "#CC79A7", "#F0E442")
cex = 1.5
cex.text = 1.5
par(lwd=2, lend=2, cex=cex, cex.lab=cex.text, cex.main=cex.text, oma=c(0, 0 ,0,0), mar=c(2.6,2.7,1.5,0.2), mgp=c(1.4,0.4,0))
	plot(y=subpow$aSPU, x=betas[subpow$beta,1], xlab='Beta', ylab='Power',
		type='n', ylim=c(0,1), main=paste('n =',n), bty='l')
	trash = sapply(names(subpow)[1:(ncol(subpow)-1)], function(x) points(betas[subpow$beta,1], subpow[,x],
		col=cbPalette[shapes[which(names(subpow)==x),2] ], pch=shapes[which(names(subpow)==x),1], type='b') )
	segments(x0=-0.1, y0=0.05, x1=0, y1=0.05)
  if(n==200){
  legend('bottomright', legend=names(subpow)[1:(ncol(subpow)-1)], col=cbPalette[ shapes[1:(ncol(subpow)-1),2] ], pch=shapes[1:(ncol(subpow)-1), 1],
         y.intersp=0.9, bg='white', bty='n', cex = 1.2)
  }
dev.off()

### WRITE OUT SURFACE MAPS FOR VOXELWISE POWER ###
# write out true vector
beta = ifelse(beta.unscaled<0, beta.unscaled*1, beta.unscaled*2)
rh$pc = beta[match( paste('R', rh$vid, sep=''), inds ) ]
rh$pc[ is.na(rh$pc) ] = 0
# save as ascii
pcout = file.path(maxsumdir, paste('rh.n', n, '_mu.asc', sep='' ) )
write.table(rh, file=pcout, col.names=FALSE, row.names=FALSE)
# convert ascii to mgh
cmds = paste('mris_convert -c', pcout, rh.sphere, sub('.asc$', '.mgh', sub(' ', '', pcout)) )
system(cmds)

if(getfwer){
	# combines them all into one data frame
		    powout = sub('setup_', '', sub('.rdata', paste('_beta', voxelwiseb, '.rdata', sep=''), sub('power_files', 'power_results', setupfile) ) )
	    load(powout)
	 
	fullpval.list = abind(fullpval.list,
		apply(fullpval.list[,ncol(pval.list),], 2, p.adjust, method='holm'),
		apply(fullpval.list[,ncol(pval.list),], 2, p.adjust, method='fdr'), along=2 )
	    colnames(fullpval.list) = c(colnames(pval.list), 'holm', 'fdr')
	    powers = rowMeans(fullpval.list<0.05, dims=2)
	    # write out simulation results
	    for( corr in colnames(powers) ){
		# only right hemisphere :)
		M = powers[, corr]
		rh$pc = M[match( paste('R', rh$vid, sep=''), inds ) ]
		rh$pc[ is.na(rh$pc) ] = 0
		# save as ascii
		pcout = file.path(maxsumdir, paste('rh.n', n, '_bunif', paste(bunif, collapse='-'), '_beta', paste(betas[voxelwiseb,], collapse='-'), '_', corr, '.asc', sep='' ) )
		write.table(rh, file=pcout, col.names=FALSE, row.names=FALSE)
		# convert ascii to mgh
		cmds = paste('mris_convert -c', pcout, rh.sphere, sub('.asc$', '.mgh', sub(' ', '', pcout)) )
		system(cmds)
		
	    }

	### FWE and FDR ###
	# FWE and FDR for the true vector
	FWE = colMeans(apply(fullpval.list[ beta==0, , ]<0.05, 2, function(x) apply(x, 2, any, na.rm=TRUE) ))
	FDR = rowMeans(colSums(fullpval.list[ beta==0, , ]<0.05, dims=1))
	FDR = FDR/rowMeans(colSums(fullpval.list[ , , ]<0.05, dims=1))
	TPR = rowMeans(colSums(fullpval.list[ beta!=0, , ]<0.05, dims=1))
	TPR = TPR/sum(beta != 0)

	# FWE and FDR for projected score
	# indicator for whether there is signal in the projected data
	signalfunc = function(basis) ifelse(rowSums(basis[,apply((basis * beta.unscaled) != 0, 2, any), drop=FALSE] ) != 0, 1, 0)
	#FWEs and FDRs for projected scores in each simulated sample
	anatinds = grep("^D", names(bases))
	ERs = array(NA, dim=c(2, length(anatinds), nsim) )
	for(s in 1:nsim){
		load(sub('setup_', '', sub('.rdata', paste('_beta', voxelwiseb, '_simid', s, '.rdata', sep=''), sub('power_files', 'power_results', setupfile) ) ) )
		# indices for this simulation where there is projected signal
		sigvec = lapply(bases[anatinds], signalfunc)
		ERs[1,,s] = sapply(1:length(sigvec), function(x) any(fullpval.list[ sigvec[[x]]==0, anatinds[x], s]<0.05 ) )
		ERs[2,,s] = sapply(1:length(sigvec), function(x) sum(fullpval.list[ sigvec[[x]]==0, anatinds[x], s]<0.05)/max(c(1,sum(fullpval.list[ , anatinds[x], s]<0.05) ) ) )
		if(! s %% 20) cat(s, '\n')
	}
	projectedERs = rowMeans(ERs, dims=2)
	colnames(projectedERs) = names(bases)[anatinds]
	rownames(projectedERs) = c('FWE', 'FDR')

	origERs = rbind(FWE, FDR, TPR)
	colnames(origERs) = colnames(powers)
	outfile = gsub('setup', 'power_results_beta0', setupfile)
	save.image(outfile)
}
