# maxkat_nullsim
# simulation under a few alternative hypotheses
# simulates the right hemisphere of imaging data
rm(list=ls())
setwd('/home/simonv/maxsum/power_analyses')
#setwd('~/Documents/work/maxsum/power_analyses')
logdir = './logdir'
dir.create(logdir, showWarnings=FALSE)
# removes old log files
unlink(file.path(logdir, '*'))
source('maxsum_functions.R')

# load freesurfer data
img = readRDS('/project/taki/AD/ADNI_FS/subject_lists/imaging/n839_thickness_demog.rds')
# to simulate numbers similar to proportions in ADNI
ps = table(img$Diagnosis)[-1]
ps = ps/sum(ps)
# remove the left hemisphere
img = img[ ,grep('^L[0-9]', names(img), invert=TRUE) ]
inds = grep('^R[0-9]', names(img), value=TRUE )
# ignore these vertices which are all zero
indsremove = inds[which( colSums( img[,inds] != 0 ) != nrow(img) ) ]
inds = inds[which( colSums( img[,inds] != 0 ) == nrow(img) ) ]
# subset imaging data to remove bad vertices
img = img[, - which(colnames(img) %in% indsremove)]

# number of subjects for power simulations
# less than the number of controls in the adni
n=100
# model type. Either normal or logistic
model = 'logistic'
# variance - doesn't really matter since variance is treated as unknown 
sigma = 1
# number of simulations for each power level
nsim=1000
# number of simulations to compute the integral
# for maxsum posthoc inference
nsimint = 2000
# uniform limits to generate nonzero parameters
# formula is (beta.unscaled + beta/unscaled/2 * runif(length(beta.unscaled), bunif[1], bunif[2]) ) * curbeta
bunif = c(-1,1)
# rejection threshold
alpha=0.05
seed=2016

# rank of PCA bases
adaptrs = 1:30
rs=c(5,10,20)
# rank for Desikan-Killiany atlas
dkrs = c(5, 10, 20, 34)
# rank for Destrieux atlas
drs = c(5, 10, 20, 50)

# create clusters of signal
# first two are negative, second is positive
clusters = c('S_temporal_sup', 'S_front_sup', 'G_and_S_cingul-Ant')
clustersigns = c(-1, -1, 1)
clusters = file.path('fsaverage5rois', paste('rh.', clusters, '.label', sep='') )
# vertex indices and sign for coefficients
csigns = do.call(rbind, lapply(1:length(clusters), function(x) {out = read.table(clusters[x], header=FALSE, skip=2, col.names=c('vertex', 'x', 'y', 'z', 'label') );
						out$sign = clustersigns[x]; out} ) )


### CREATE BASES BASED ON STRUCTURAL ATLASES ###
set.seed(seed)
aparcrois = system( 'ls fsaverage5rois/*aparc.[a-z]*', intern=TRUE)
aparcrois = aparcrois[ grep('corpuscallosum|unknown', aparcrois, invert=TRUE) ]
# vertex indices and sign for coefficients
aparc = do.call(rbind, lapply(1:min(length(aparcrois), max(drs)), function(x) {out = read.table(aparcrois[x], header=FALSE, skip=2, col.names=c('vertex', 'x', 'y', 'z', 'label') );
						out$label = x; out} ) ) # labels are just numbers here, but could make them the name of the ROI
# some vertices in the insula were excluded from analysis due to zeros in the observed data I think
# this command removes them from the basis
aparc = aparc[ -which( ! aparc$vertex %in% as.numeric(sub('R', '', inds)) ), ]

aparc2009rois = system( 'ls fsaverage5rois/rh*2009s*', intern=TRUE)
aparc2009rois = aparc2009rois[ grep('Medial_wall', aparc2009rois, invert=TRUE) ]
aparc2009 = do.call(rbind, lapply(1:length(aparc2009rois), function(x) {out = read.table(aparc2009rois[x], header=FALSE, skip=2, col.names=c('vertex', 'x', 'y', 'z', 'label') );
						out$label = x; out} ) )

# for estimates of some covariance structure to use to simulate covariance within
# the regions. Need the full sample so that the covariance matrices are full rank
vnames1 = gsub(" ", "", paste('R', csigns$vertex[ csigns$sign<0], ''))
sigma1 = cov(apply(img[, vnames1], 2, function(x) resid(lm(x ~ ADNI_MEM, data=img)) ) )
sigma1sqrt = svd(sigma1, nv=0)
sigma1sqrt = t(t(sigma1sqrt$u) * sqrt(sigma1sqrt$d))
# positive signal covariance independent of negative signal
vnames2 = gsub(" ", "", paste('R', csigns$vertex[ csigns$sign>0], ''))
sigma2 = cov(apply(img[, vnames2], 2, function(x) resid(lm(x ~ ADNI_MEM, data=img)) ) )
sigma2sqrt = svd(sigma2, nv=0)
sigma2sqrt = t(t(sigma2sqrt$u) * sqrt(sigma2sqrt$d))


# RANGE OF BETAS TO CONSIDER
# first column is coefficient for negative clusters
# second column is for the positive cluster
# to compute probabilities. 0.57 corresponds to baseline log(ODDS) of MCI in the sample
# invlogit = function(beta) 1/(1 + exp(- (0.57 + beta*669 - beta*191) ) )
# invlogit = function(beta) 1/(1 + exp(- (0.57 + beta[1]*669 - beta[2]*191) ) )
vec = c(0, 0.5, 0.75, 1, 1.5, 2, 2.5, 3, 3.5, 4)
betas = cbind(vec, vec*2)*10^{-3}
# range(apply(betas, 1, invlogit))
beta.unscaled = rep(0, length(inds))
beta.unscaled[match(csigns$vertex, as.numeric(sub('R', '', inds)) )] = csigns$sign



# Now we can subset to controls
img = img[ img$Diagnosis == 'CN' & !is.na(img$ADNI_MEM),]

#### THINGS THAT ARE FIXED ####
# fixed design
# might have to center
# consider using spatially correlated variables

# save out objects we will use
dir.create('power_files', showWarnings = FALSE)
setupfile = paste('power_files/setup_n', n, '_model', model, '_sigma', sigma, '_alpha', alpha, '_bunif',  paste(bunif, collapse='-'), '.rdata', sep='' )

  #### empty simulation results matrix ####
# this command requires the same number of bases for each
# type of basis. The 3 at the end of command is the number of types
# of bases. Covariance basis, and two atlases bases.
  R1nams = paste(c('PCAadapt', paste('PCA', rs, sep='_'),
		paste('Destrieux', drs, sep='_'),
		paste('DK', dkrs, sep='_')), 
		rep(c('', '_pvalue'), each=1+length(rs)+length(drs)+length(dkrs)), sep='' )
  nams = c(R1nams, 'aSPU', 'aSPU_pvalue', 'SPU2', 'SPU2_pvalue', 'SPU1', 'SPU1_pvalue', 'SPUInf', 'SPUInf_pvalue')
  # 'SKAT', 'SKAT_pvalue', # same as SPU2
  simresults = matrix(NA, nrow=1, ncol=length(nams), dimnames=list(NULL, nams))
 

  #### Empty power results ####
  powresults = matrix(NA, nrow=nrow(betas), ncol=(1+sum(grepl('pvalue', nams) )), dimnames=list(NULL, c( sub('_pvalue', '', grep('_pvalue$', nams, value=TRUE) ), 'beta') ) )
  powresults = as.data.frame(powresults)
  
  
  # save out info so far for this value of k
  save.image( file=setupfile )



dir.create('power_results', showWarnings = FALSE)
#### submits array job for the betas ####
for(betaind in 1:nrow(betas)){
	#curbetas = betas[betaind, ] 
	#beta = c(rep(-curbetas[1], nrow(sigma1sqrt)), rep(-curbetas[2], nrow(sigma2sqrt)) )
	cat(betas[ betaind,], '\n')
	system( paste( '/lsf/9.1/linux2.6-glibc2.3-x86_64/bin/bsub -q taki_normal -o', logdir, '-e', logdir, paste('-J maxsum_altsim[1-', nsim, ']', sep=''),  '/appl/R-3.2.3/bin/R --slave --file=./maxsum_altsim_imaging.R --args', getwd(), setupfile, betaind) )
}
