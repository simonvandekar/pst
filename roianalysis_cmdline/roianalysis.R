#!/appl/R-3.1.3/bin/Rscript --vanilla --no-init-file
#### LOAD OPTPARSE ####
suppressMessages(require(optparse))



### ARGUMENTS ###
ol = list(make_option(c("-i", "--imagedata"), action="store", default=NULL, help="Path to 4D nifti or CSV/RDS file. If a nifti each volume is assumed to be a subject. If a CSV file each row is a subject, the first column is subject id, and each column after is an ROI. Subjects are assumed to be in the same order as the data file of covariates. Required argument."),
	    make_option(c("-d", "--datafile"), action="store", default=NULL, help="Data file (either CSV or RDS) that contains phenotype/outcome variable and any covariates to control for. Can have extra columns that won't be used in the analysis. Assumed to be in the same order as imagedata. Required argument."),
	    make_option(c("-e", "--equation"), action="store", default=NULL, help="Model equation for covariates in standard R form \"outcome ~ covariate1 + ... + covariatem\". If there are no covariates then specify \"outcome ~ 1\". Covariate and outcome names must match column names in datafile given above. Required argument."),
	    make_option(c("-f", "--family"), action="store", default='gaussian', help="Family for outcome variable. See \"?family\" in R. Known to work for binomial, gaussian, and quasipoisson. Default is gaussian."),
	    make_option(c("-l", "--label"), action="store", default=NULL, help="A 3D label nifti. A 3D nifti image will be written with corrected -log10(p) values for each of the ROIs. If input is a CSV file then  a key must be given linking the column names of \"imagedata\" to the label numbers in \"label\". Otherwise, columns of the input are assumed to correspond to label numbers in the image. If input is a 4D nifti then analysis is performed on averages within the regions defined by the labels, and a 3D nifti is written with corrected -log10(p) values. Required if input is a 4D nifti."),
	    make_option(c("-k", "--key"), action="store", default=NULL, help="If imagedata is a CSV or RDS file then this is a key that maps the label numbers to thecolumn names of image data. This should be a CSV or RDS file with two columns. One column must be a string or factor and the other numeric/integer. Optional argument."),
	    make_option(c("-m", "--mask"), action="store", default=NULL, help="If input is a 4D nifti then this is the mask in which to perform analyses. Optional argument."),
	    make_option(c("-n", "--nsim"), action="store", default=5000, help="Number of simulations for Monte Carlo integration. Default is 5000."),
	    make_option(c("-o", "--output"), action="store", default=NULL, help="Path including name of new directory where output will be placed. Required argument.")
)

#### LOAD LIBRARIES ####
suppressMessages(require(oro.nifti))
suppressMessages(require(splines))



#### FUNCTIONS ####
# copies hdr from img1 to img2
# for writing out files in the correct orientation
copyhdr = function(img1, img2){
atts = c("pixdim","qform_code","sform_code","quatern_b","quatern_c","quatern_d","qoffset_x","qoffset_y","qoffset_z","srow_x","srow_y","srow_z","reoriented")
attributes(img2)[atts] = attributes(img1)[atts]
img2
}

#### POSTHOC INFERENCE FOR LABELS ####
# this is faster than for any other basis
# because the problem is more simple with indicator bases
maxsum.posthoc.label = function(m, GQ, r, nsimint=5000, alpha=0.05){
	# actual value is one of the "simulated" values
	nsimint=nsimint-1
	Y = resid(m, type='response')
	X = model.matrix(m)
	Gamma = diag(Y^2)
	covY = ( Gamma - Gamma %*% X %*% solve( t(X) %*% Gamma %*% X) %*% t(X) %*% Gamma )
	# Things for the variance
	V = t(GQ) %*% covY %*% GQ
	# Vsqrt is standardized here
	Vsqrt = svd(cov2cor(V), nv=0, nu=r)
	Vsqrt = Vsqrt$u %*% diag(sqrt(Vsqrt$d), r) %*% t(Vsqrt$u)
	# sqrt of diagonal of Q V Q^T (covariance of the projected U vector)
	diagcov = sqrt(diag(V))
	diagcov = ifelse(diagcov==0, 1, diagcov)

	# observed mean values in each region
	PU = t(GQ) %*% Y
	# These are technically rotated scores, not projected
	PUstd = c(PU/diagcov)

	# Vsqrt is standardized
	infnorm = function(z){
	  max(abs(Vsqrt %*% z))
	}

	# Monte carlo integration -- or just simulation under the null
	cat('performing simulations\n')
	simnormals = matrix(rnorm(nsimint*r), ncol=r, nrow=nsimint)
	siminftynorms = apply(simnormals, 1, infnorm)
	# add observed data plus small epsilon to avoid log10(0)
	Fb = ecdf(c(siminftynorms, infnorm(PUstd) + 10^{-8}) )
	# define a quantile function and compute voxelwise pvalues (and return the result)
	return(list(PUstd = PUstd, rejreg = quantile(siminftynorms, probs=1-alpha), pvals = 1-Fb(abs(PUstd)), Fb=Fb ))
}



opt = parse_args(OptionParser(option_list=ol))

#opt = list()
#opt$equation = "group ~ ns(age, df=5) + sex + white + meduCnbGo1 + icv"
#opt$family='binomial'
#opt$datafile = '~/maxsum/roianalysis_cmdline/4d/n791_psTdT1dataForSimon_20160506.rds'
#opt$imagedata = "~/maxsum/roianalysis_cmdline/4d/n791_ROIdata_20160506.csv"
#opt$mask = NULL
#opt$output = "~/maxsum/roianalysis_cmdline/pncCSV"
#opt$label = "/project/taki2/templates/dramms/dramms_ravens_template_1mm_labels.nii.gz"
#opt$key = "~/maxsum/roianalysis_cmdline/4d/multiatlas_gmd_lookup.csv"
#opt$nsim=5000
#cat(str(opt))

#### CREATE OUTPUT DIRECTORY ####
if(is.null( opt[[ 'output' ]])){
	stop('Must include output directory.')
}
out = opt$output
dir.create(out, showWarnings=FALSE, recursive=TRUE)
# newly created images
out.tab = file.path(out, 'results_table.csv')
out.pval = file.path(out, 'negative_log10_pvalue.nii.gz')
out.uncor = file.path(out, 'negative_log10_uncor.nii.gz')
out.fdr = file.path(out, 'negative_log10_fdr.nii.gz')
out.bonf = file.path(out, 'negative_log10_bonf.nii.gz')
out.zval = file.path(out, 'standardized_scores.nii.gz')
# save these images
#out.mask = file.path(out, 'mask.nii.gz')
# text file for statistical output
out.stats = file.path(out, 'score_test.txt')
# rdata file for R objects of interest
out.rdata = file.path(out, 'useful_objects.rdata')


#### FILE AND ARGUMENT CHECKS ####
if( is.null( opt[[ 'imagedata' ]] ) ){
	stop('Must include input data.')
}


# check that files exist
images = c('imagedata', 'label', 'mask', 'datafile', 'key')
images = images[ !sapply(opt[images], is.null)]
if( any(! file.exists(unlist( opt[images] ) ) ) ){
	stop('One of the file arguments doesn\'t exist.')
}

# check if input is a nifti
if( grepl('.nii$|.nii.gz', tolower(opt$imagedata))){
	nift = TRUE
	images = 'imagedata'
} else{
	nift = FALSE
	images = NULL
}

# check image dimensions
# uses c3d and parses standard output
# check if a mask was passed
if( !is.null(opt$mask)){
	images = c(images, 'mask')
}
# check if the basis command is an image
if( !is.null(opt$label)){
	images = c(images, 'label')
}
# check if image dimensions are all the same
# (if there are any images given)
if( ! is.null(images)){
	cmd = paste('c3d', paste(c(rbind(unlist(opt[ images ]), '-info')), collapse=' '), '| cut -d ";" -f1 | cut -d":" -f2')
	dims = system(cmd, intern=TRUE)
	# removes white space
	dims = dims[ grep("^$", dims, invert=TRUE)]
	if(length(unique(dims)) > 1){
		stop(paste('Images dimensions do not all match.', paste(dims, collapse=';')) )
	}
}


# read in data file
if(grepl('.csv$', tolower(opt$datafile))){
	dat = read.csv(opt$datafile)
}else if(grepl('.rds$', tolower(opt$datafile)) ){
	dat = readRDS(opt$datafile)
}else{
	stop('I don\'t know how to read this datafile!')
}

#### CHECK FOR COVARIATES ####
f0 = as.formula(opt$equation)
covs = all.vars(f0)
if( ! all( covs %in% names(dat))){
	stop('Some covariates in formula aren\'t in datafile.')
}
nadatrows = which(apply(is.na(dat[,covs]), 1, any))



# 4D subject file
cat('****** LOADING IMAGING DATA ******\n')
if(nift){
	imgloadtime = system.time(imgdat <- readNIfTI( opt$imagedata))
	cat('Data load time:', round(as.table(imgloadtime)[3]/60, 2), 'minutes\n', sep=' ')
} else {
	if(grepl('.csv$', tolower(opt$imagedata))){
		imgdat = read.csv(opt$imagedata)
	}else if(grepl('.rds$', tolower(opt$imagedata)) ){
		imgdat = readRDS(opt$imagedata)
	}else{
		stop('I don\'t know how to read this inputfile! Should end in csv or rds (if not a nifti) and be of the appropriate format.')
	}
	naimgrows = which(apply(is.na(imgdat), 1, any))
	narows = unique(c(naimgrows, nadatrows))
	# remove rows from imaging data where any variables are missing
	imgdat = imgdat[ -narows,]
	s = as.matrix(imgdat[,-1])
	r = ncol(s)
}

### EXCLUDING SUBJECTS WITH MISSING DATA ###
if(length(narows)>0){
	cat('Excluding', length(naimgrows), 'subjects with missing imaging data!\n')
	cat('Excluding additional', sum(! nadatrows %in% naimgrows ), 'subjects with missing covariates!\n')
	dat = dat[ -narows, ]
}

# read in key
if( !is.null(opt$key)){
	if(grepl('.csv$', tolower(opt$key))){
		key = read.csv(opt$key)
	}else if(grepl('.rds$', tolower(opt$key)) ){
		key = readRDS(opt$key)
	}else{
		stop('I don\'t know how to read this key!')
	}
	# get a numeric and a non-numeric column from the key file
	# if there isn't only one of each then it errors.
	labcol = which(sapply(key, is.numeric))
	namecol = which(sapply(key, function(x) !is.numeric(x)))
	if( length(labcol) == 1 & length(namecol) == 1){
		key[, c(labcol, namecol) ]
	} else {
		stop('I don\'t know how to read the columns of this key!')
	}
	# checks to see if there are column names not in the key
	if( any(! colnames(s) %in% key[,1] )){
		stop('There are column names in the imaging data that are not in the key!')
	} else {
		key = key[match( colnames(s), key[,1]), ]
	}
}

# read in label data
if( ! is.null(opt$label)){
	#system(paste('cp', opt$label, out.label))
	labeldat = readNIfTI(opt$label)
	labels = c(labeldat@.Data)
}
	

# masks image
if( ! is.null(opt$mask) & nift){
	system(paste('cp', opt$mask, out.mask))
	maskdat = readNIfTI( opt$mask)
	# replaces nifti object with vector of voxels within mask
	imgdat = apply(imgdat@.Data, 4, c)[ c(maskdat@.Data)!=0, ]
	labels = labels[ c(maskdat@.Data)!=0 ]
}


#### NULL MODEL ####
# does not involve imaging data
m0 = glm(f0, data=dat, family=opt$family)


#### ANATOMICAL BASIS ####
if( !is.null(opt$label) ){
	# removes 0
	uq = sort(unique(labels))[-1]
	if( nift ){
		# number of labels
		r = length(uq)
		# maxsum posthoc procedure
		# GQ is mean in region multiplied by square root of number of
		# voxels in the region, this command ignores the square root term
		s = sapply(uq, function(uqi){ colMeans( imgdat[which(basisdat==uqi), ]) } )
		key = data.frame(labids=uq, imgdatcols=uq)
	}
	# check if length of labels equals number of ROIs
	if( r != length(uq) & is.null(opt$key) ){
		cat('Number of labels in nifti does not equal the number of ROIs in the input file.\n Just writing output csv instead of nifti.\n')
	} else {
		# use the key to match the labels
		# there can be NAs here
		index = data.frame(labids=uq, imgdatcols=match( uq, key[,2] ))
	}
}


cat('****** PERFORMING MS POSTHOC ******\n')
posthoctime = system.time(ms.posthoc <- maxsum.posthoc.label( m=m0, GQ=s, r=r, nsimint=opt$nsim ) )
cat('Time to perform MS posthoc:', round(as.table(posthoctime)[3]/60, 2), 'minutes\n', sep=' ')
fs = update.formula(f0, '. ~ . + x')
stdinf.out = as.data.frame(t(apply(s, 2, function(x){dat$x = x; summary(glm(fs, data=dat, family=opt$family))$coefficients['x', c('z value', 'Pr(>|z|)')] })))
stdinf.out[, 'FDR'] = p.adjust(stdinf.out[,2], method='fdr') 
stdinf.out[,'Bonferroni'] = p.adjust(stdinf.out[,2], method='bonf') 



# alternative model
f1 = update.formula(f0, '. ~ . + s')
m1 = glm(f1, data=dat, family=opt$family)
ms.test = anova(m0, m1, test='Rao')

# write images
if( !is.null(opt$label) ){
	cat('****** WRITING OUTPUT ******\n')
	# This is for the imaging output
	log10.out = -log10(stdinf.out[c('Pr(>|z|)', 'FDR', 'Bonferroni') ]) * sign(stdinf.out[,1])
	log10.out = cbind( sign(ms.posthoc$PUstd) * -log10(ms.posthoc$pvals ), log10.out)
	index[,c('msCorrpvalue', 'uncorrected', 'FDR', 'Bonferroni')] = log10.out[index[,'imgdatcols'], ]
	index[, 'PUstd'] = ms.posthoc$PUstd[ index[,'imgdatcols'] ]
	index[ is.na(index) ] = 0

	cmds = c(paste('c3d', opt$label, '-replace', paste(c(rbind(index[,1], index[,3] ) ), collapse=' '), '-o', out.pval ),
		paste('c3d', opt$label, '-replace', paste(c(rbind(index[,1], index[,6] ) ), collapse=' '), '-o', out.bonf ),
		paste('c3d', opt$label, '-replace', paste(c(rbind(index[,1], index[,5] ) ), collapse=' '), '-o', out.fdr ),
		paste('c3d', opt$label, '-replace', paste(c(rbind(index[,1], index[,4] ) ), collapse=' '), '-o', out.uncor ),
		paste('c3d',opt$label, '-replace', paste( c(rbind(index[,1], index[,7] ) ), collapse=' ' ), '-o', out.zval ) )
	trash = sapply(cmds, system)
} else {
	outtab = data.frame(ROI=colnames(s), 'standardizedscore'=ms.posthoc$PUstd, 'msCorrpvalue'=ms.posthoc$pvals)
	outtab[, c('uncorrected', 'FDR', 'Bonferroni')] = stdinf.out[, c('Pr(>|z|)', 'FDR', 'Bonferroni')]
	write.csv( outtab, file=out.tab, quote=FALSE, row.names=FALSE)
}

# stats output
capture.output(ms.test, file=out.stats, append=FALSE)
save(m0, m1, ms.test, naimgrows, nadatrows, ms.posthoc, file=out.rdata)
